# Note Service

[![pipeline status](https://gitlab.com/advprog_12-unus/notesunus/badges/master/pipeline.svg)](https://gitlab.com/advprog_12-unus/notesunus/-/commits/master)
[![coverage report](https://gitlab.com/advprog_12-unus/notesunus/badges/master/coverage.svg)](https://gitlab.com/advprog_12-unus/notesunus/-/commits/master)

Hanya microservice dan harap dijalankan bersama registry
sebelum membuka main service

Deployed on : http://unus-note.herokuapp.com/