package com.unus.noteservice;

import com.unus.noteservice.repository.NoteRepository;
import com.unus.noteservice.service.NoteServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
class NoteserviceApplicationTests {

    @MockBean
    private NoteRepository noteRepository;

    @MockBean
    private NoteServiceImpl noteService;

    @Test
    void contextLoads() {
    }

    @Test
    public void mainFunctionTest() {
        NoteserviceApplication.main(new String[] {});
    }
}
