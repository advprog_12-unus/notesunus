package com.unus.noteservice.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NoteBaseTest {
    private NoteBase noteTest;

    @BeforeEach
    void setUp() {
        noteTest = new NoteBase();
        noteTest.setTitle("TitleTest");
        noteTest.setDesc("DescTest");
        noteTest.setUser((long)1);
    }

    @Test
    void setTitleTest() {
        noteTest.setTitle("Title2");
        assertEquals("Title2", noteTest.getTitle());
        noteTest.setTitle("TitleTest");
    }

    @Test
    void setDescTest() {
        noteTest.setDesc("Desc2");
        assertEquals("Desc2", noteTest.getDesc());
        noteTest.setDesc("DescTest");
    }

    @Test
    public void setUserTest() {
        noteTest.setUser((long)2);
        assertEquals(2, noteTest.getUser());
        noteTest.setUser((long)1);
    }

    @Test
    public void getUserTest() {
        assertEquals(1, noteTest.getUser());
    }

    @Test
    void getIdTest() {
        assertEquals(0, noteTest.getId());
    }

    @Test
    void getTitleTest() {
        assertEquals("TitleTest", noteTest.getTitle());
    }

    @Test
    void getDescTest() {
        assertEquals("DescTest", noteTest.getDesc());
    }

}