package com.unus.noteservice.service;

import com.unus.noteservice.core.NoteBase;
import com.unus.noteservice.repository.NoteRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class NoteServiceImplTest {
    @Mock
    private NoteRepository noteRepo;

    private NoteBase noteTest;

    @InjectMocks
    private NoteServiceImpl noteService;

    @BeforeEach
    void setUp() {
        noteTest = new NoteBase();
        noteTest.setTitle("TestingTitle");
        noteTest.setDesc("TestingDescription");
    }

    @Test
    void registerTest() {
        noteService.register(noteTest);
        lenient().when(noteService.register(noteTest)).thenReturn(noteTest);
    }

    @Test
    void getAllNotesOfUserTest() {
        List<NoteBase> noteList = noteService.getAllNotesOfUser((long)1);
        lenient().when(noteService.getAllNotesOfUser((long)1)).thenReturn(noteList);
    }

    @Test
    void deleteNoteTest() {
        noteService.register(noteTest);
        noteService.deleteNote(noteTest.getId());
        lenient().when(noteService.register(noteTest)).thenReturn(noteTest);
    }
}