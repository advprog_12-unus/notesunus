package com.unus.noteservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unus.noteservice.core.NoteBase;
import com.unus.noteservice.repository.NoteRepository;
import com.unus.noteservice.service.NoteServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultHandler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = NotesController.class)
class NotesControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private NoteRepository noteRepository;

    @MockBean
    private NoteServiceImpl noteService;

    @Test
    private void allNoteTest() throws Exception {
        mockMvc.perform(get("/service-notes/allNotes/1}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(new ResultHandler() {
                    @Override
                    public void handle(MvcResult result) throws Exception {
                        assertEquals("[]",result.getResponse().getContentAsString());
                    }
                });
    }

    @Test
    void createNoteTest() throws Exception {
        NoteBase noteTest = new NoteBase();
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(noteTest);

        mockMvc.perform(post("/service-notes/create-note").content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void addNote() throws Exception {
        NoteBase noteTest = new NoteBase();
        noteService.register(noteTest);
        lenient().when(noteService.register(noteTest)).thenReturn(noteTest);
        mockMvc.perform(get("/service-notes/add-note/1"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void deleteNote() throws Exception {
        mockMvc.perform(delete("/service-notes/delete-note/1"))
                .andExpect(status().is4xxClientError());
    }
}