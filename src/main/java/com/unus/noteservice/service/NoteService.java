package com.unus.noteservice.service;

import com.unus.noteservice.core.NoteBase;

import java.util.List;

public interface NoteService {
    public void deleteNote(Long id); //delete

    public NoteBase register(NoteBase notif); //create

    public List<NoteBase> getAllNotesOfUser(Long idUser);
}
