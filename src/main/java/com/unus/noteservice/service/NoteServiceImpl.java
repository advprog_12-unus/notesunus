package com.unus.noteservice.service;

import com.unus.noteservice.core.NoteBase;
import com.unus.noteservice.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteServiceImpl implements NoteService {
    @Autowired
    private NoteRepository noteRepo;

    public NoteServiceImpl(NoteRepository notesRepo) {
        this.noteRepo = notesRepo;
    }

    @Override
    public NoteBase register(NoteBase note) {
        return noteRepo.save(note);
    }

    @Override
    public List<NoteBase> getAllNotesOfUser(Long idUser) {
        return noteRepo.findAllByUserid(idUser);
    }

    @Override
    public void deleteNote(Long id) {
        noteRepo.deleteById(id);
    }

}
