package com.unus.noteservice.controller;

import com.unus.noteservice.core.NoteBase;
import com.unus.noteservice.repository.NoteRepository;
import com.unus.noteservice.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/service-notes")
public class NotesController {

    @Autowired
    private NoteService noteService;

    @Autowired
    private NoteRepository noteRepository;

    public NotesController(NoteService noteService) {
        this.noteService = noteService;
    }

    @GetMapping("/allNotes/{idUser}")
    private List<NoteBase> allNote(@PathVariable("idUser") Long idUser) {
        return noteService.getAllNotesOfUser(idUser);
    }

    // Route ke form note
    @GetMapping("/create-note")
    public NoteBase createNote(Model model) {
        return new NoteBase();
    }

    /** Route untuk menyimpan note yang ditambahkan dan kembali ke halaman list notes. */
    @PostMapping("/add-note/{idUser}")
    public void addNote(@PathVariable("idUser") Long idUser, @RequestBody NoteBase note) {
        note.setUser(idUser);
        noteService.register(note);
    }

    // Route untuk membuang note yang dipilih
    @GetMapping("/delete-note/{id}")
    public void deleteNote(@PathVariable Long id) {
        noteService.deleteNote(id);
    }
}
