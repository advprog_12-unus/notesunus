package com.unus.noteservice.repository;

import com.unus.noteservice.core.NoteBase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoteRepository extends JpaRepository<NoteBase, Long> {
    List<NoteBase> findAllByUserid(Long userid);
}
